#!/bin/sh
#
# Install script for Nextcloud inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

# Packages only used in the build phase.
BUILD_PACKAGES="
"

# Packages required to serve the website and run the services, in
# addition to those already installed by the base apache2 image.
PACKAGES="
	libapache2-mod-sso
	php-apcu
	php-bcmath
	php-bz2
	php-cli
	php-curl
	php-gd
	php-gmp
	php-igbinary
	php-intl
	php-json
	php-memcached
	php-mysql
	php-redis
	php-xml
	php-zip
"

# Apache modules to enable.
APACHE_MODULES_ENABLE="
	env
	headers
	mime
	rewrite
	setenvif
	sso
	unique_id
"

# Sites to enable.
APACHE_SITES="
	nextcloud
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
install_packages() {
    env DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install -y --no-install-recommends "$@"
}

set -e
umask 022

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Install our little 'occ' wrapper.
cat >/usr/bin/occ <<EOF
#!/bin/sh
cd /var/www/nextcloud
exec php occ "\$@"
EOF
chmod 0755 /usr/bin/occ

# Setup Apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2ensite ${APACHE_SITES}

# Ensure that the startup script is executable.
chmod 755 /start.sh

# Remove packages used for installation.
apt-get remove --purge ${BUILD_PACKAGES}
apt-get clean
rm -fr /var/lib/apt/lists/*

