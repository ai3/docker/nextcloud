FROM debian:stable AS build

ENV NEXTCLOUD_VERSION=19.0.0

ADD . /build
RUN /build/install-nextcloud.sh

FROM registry.git.autistici.org/ai3/docker/apache2-php-base:master

COPY --from=build /var/www/nextcloud /var/www/nextcloud
COPY htaccess /var/www/nextcloud/.htaccess
COPY conf/ /etc/
COPY build.sh /tmp/build.sh
COPY start.sh /start.sh
RUN /tmp/build.sh && rm -fr /tmp/build.sh

