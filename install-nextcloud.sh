#!/bin/sh

if [ -z "$NEXTCLOUD_VERSION" ]; then
    echo "NEXTCLOUD_VERSION is unset!" >&2
    exit 1
fi

# We will automatically download the latest release of the apps,
# compatibly with the given NEXTCLOUD_VERSION.
NEXTCLOUD_APPS="
	user_saml
	calendar
	circles
	contacts
	files_antivirus
"

# Packages required to serve the website and run the services, in
# addition to those already installed by the base apache2 image.
PACKAGES="
	patch
	curl
	ca-certificates
	bzip2
	tar
	python3
	python3-packaging
	python3-requests
"

install_packages() {
    env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
}

set -e
umask 022

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Download nextcloud tarball and unpack it into /var/www/nextcloud.
echo "Downloading and extracting tarball..."
mkdir -p /var/www
dl_uri="https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2"
curl -sL "${dl_uri}" \
    | bunzip2 -c \
    | tar -C /var/www -xf -

# Install apps into the apps/ directory.
python3 /build/install-nextcloud-apps.py --version=${NEXTCLOUD_VERSION} ${NEXTCLOUD_APPS}

chown -R root.root /var/www/nextcloud

# Apply patches.
for p in /build/patches/*.patch ; do
    pname=$(basename "$p")
    echo "Applying patch $pname..."
    patch --directory=/var/www/nextcloud --batch -p1 -l -E < "$p"
done

# Remove the existing config dir contents.
rm -fr /var/www/nextcloud/config/*

echo "Nextcloud ${NEXTCLOUD_VERSION} was installed successfully"

exit 0
