#!/bin/sh
#
# Start Nextcloud.
#
# This script is invoked before starting apache2 and the PHP
# runner. It copies the configuration files in their desired location,
# and runs database upgrades if necessary.
#

TARGET_DIR=/var/www/nextcloud
CONFIG_DIR=${TARGET_DIR}/config

# Name of the MySQL database to use.
MYSQL_DB="ai_nextcloud"

# If ${CONFIG_DIR}/my.cnf exists, use it as a defaults file for mysql.
MYSQL=mysql
if [ -e "${CONFIG_DIR}/my.cnf" ]; then
    MYSQL="${MYSQL} --defaults-file=${CONFIG_DIR}/my.cnf"
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

# Abort if some fundamental environment variables are not defined.
# This is nicer than having Apache die with an error later.
test -n "${DOMAIN}" \
    || die "the 'DOMAIN' environment variable is not defined"

# Verify that the main Roundcube configuration is in place.
test -e ${CONFIG_DIR}/config.php \
    || die "${CONFIG_DIR}/config.php is missing"

# Now set up the MySQL database for Roundcube. The current Roundcube
# version is stored in the database.
flag_file=${CONFIG_DIR}/.db_initialized
if [ ! -e "${flag_file}" ]; then

    occ maintenance:install \
	--database mysql \
	--database-name "${MYSQL_DB}" \
	--database-user nextcloud \
	--database-pass "${MYSQL_PASSWORD}" \
	--admin-user admin \
	--admin-pass "${ADMIN_PASSWORD:-password}" \
        || die "error installing Nextcloud"

    touch "${flag_file}"

fi


exit 0
